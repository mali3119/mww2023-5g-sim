* Steps to start basic OAI-based.
1. Deploy core network: `cd /opt/oai-cn5g-fed/docker-compose; sudo python3 ./core-network.py --type start-basic --scenario 1`
2. See AMF logs: `sudo docker logs -f oai-amf`
3. Execute gNB: `cd /opt/openairinterface5g/cmake_targets; sudo RFSIMULATOR=server ./ran_build/build/nr-softmodem -O /local/repository/etc/gnb.conf --sa --rfsim`
4. Execute UE: `cd /opt/openairinterface5g/cmake_targets; sudo RFSIMULATOR=127.0.0.1 ./ran_build/build/nr-uesoftmodem -O /local/repository/etc/ue.conf -r 106 -C 3619200000 --sa --nokrnmod --numerology 1 --band 78 --rfsim --rfsimulator.options chanmod --telnetsrv`
5. Start Perf: `iperf3 -s`
6. Copy the UE IP: `UEIP=$(ip -o -4 addr list oaitun_ue1 | awk '{print $4}' | cut -d/ -f1)`
7. Start downlink traffic from OAI-ext-dn: `sudo docker exec -it oai-ext-dn iperf3 -c $UEIP -t 50000`

or 

1. Ping from UE: `ping -I oaitun_ue1 192.168.70.135`

* Information
1. UE settings at `/local/repository/etc/ue.conf`

* After changing AMF code
1. `sudo DOCKER_BUILDKIT=1 docker build --target oai-amf --tag oai-amf:v1.4.0 \
               --file component/oai-amf/docker/Dockerfile.amf.ubuntu18 \
               component/oai-amf`
2. `docker image prune --force; docker container prune --force; docker volume prune --force; docker system prune`

* Login to MySQL:
1. Server IP: 192.168.70.131, user = root, password = linux, database = oai_db
2. Execute `sudo docker exec -it image_id bash`
3. Execute `mysql -u root -p`
4. Execute 'USE oai_db;'
5. To show all the tables: `SHOW TABLES;`
6. To show all colums from table: `SHOW COLUMNS from AuthenticationSubscription;`

7. 

INSERT INTO `AuthenticationSubscription` (`ueid`, `authenticationMethod`, `encPermanentKey`, `protectionParameterId`, `sequenceNumber`, `authenticationManagementField`, `algorithmId`, `encOpcKey`, `encTopcKey`, `vectorGenerationInHss`, `n5gcAuthMethod`, `rgAuthenticationInd`, `supi`) VALUES ('208950000000031', '5G_AKA', '0C0A34601D4F07677303652C0462535B', '0C0A34601D4F07677303652C0462535B', '{"sqn": "000000000020", "sqnScheme": "NON_TIME_BASED", "lastIndexes": {"ausf": 0}}', '8000', 'milenage', '63bfa50ee6523365ff14c1f45f88737d', NULL, NULL, NULL, NULL, '208950000000031');

INSERT INTO `SessionManagementSubscriptionData` (`ueid`, `servingPlmnid`, `singleNssai`, `dnnConfigurations`) VALUES ('208950000000031', '20895', '{"sst": 222, "sd": "123"}','{"default":{"pduSessionTypes":{ "defaultSessionType": "IPV4"},"sscModes": {"defaultSscMode": "SSC_MODE_1"},"5gQosProfile": {"5qi": 6,"arp":{"priorityLevel": 1,"preemptCap": "NOT_PREEMPT","preemptVuln":"NOT_PREEMPTABLE"},"priorityLevel":1},"sessionAmbr":{"uplink":"100Mbps", "downlink":"100Mbps"},"staticIpAddress":[{"ipv4Addr": "12.1.1.4"
}]}}');
